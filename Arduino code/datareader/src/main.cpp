    // MPU-6050 Short Example Sketch
    // By Arduino User JohnChi
    // August 17, 2014
    // Public Domain
    #include<Wire.h>
    #include "Arduino.h"
    #include <SD.h>

    const double acC = 9.81/16384.0;
    const double gyC = 1/131.0;
    const int MPU_addr=0x68;  // I2C address of the MPU-6050
    int16_t AcX,AcY,AcZ,Tmp,GyX,GyY,GyZ;
    int counter =0;

    const int chipSelect = 10;
    char filename[16];

    void setup(){
        Wire.begin();
        Wire.beginTransmission(MPU_addr);
        Wire.write(0x6B);  // PWR_MGMT_1 register
        Wire.write(0);     // set to zero (wakes up the MPU-6050)
        Wire.endTransmission(true);
        Serial.begin(9600);


        /*
        Serial.print("Initializing SD card...");
        pinMode(10, OUTPUT);
        if (!SD.begin(chipSelect)) {
            Serial.println("Card failed, or not present");
            return;
        }
        Serial.println("card initialized.");
        
        int n = 0;
        do{
                snprintf(filename, sizeof(filename), "data%03d.txt", n); // includes a three-digit sequence number in the file name
        }
        while(SD.exists(filename)); //now filename[] contains the name of a file that doesn't exist
        */

    }
    void loop(){
        Wire.beginTransmission(MPU_addr);
        Wire.write(0x3B);  // starting with register 0x3B (ACCEL_XOUT_H)
        Wire.endTransmission(false);
        Wire.requestFrom(MPU_addr,14,true);  // request a total of 14 registers
        AcX=Wire.read()<<8|Wire.read();  // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)    
        AcY=Wire.read()<<8|Wire.read();  // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
        AcZ=Wire.read()<<8|Wire.read();  // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
        Tmp=Wire.read()<<8|Wire.read();  // 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
        GyX=Wire.read()<<8|Wire.read();  // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
        GyY=Wire.read()<<8|Wire.read();  // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
        GyZ=Wire.read()<<8|Wire.read();  // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)
        
        
        //Serial.print((float) 1000*counter / millis());
        //counter++;
        //Serial.print(";");

        Serial.print(millis());
        Serial.print(";"); Serial.print(AcX);
        Serial.print(":"); Serial.print(AcY);
        Serial.print(":"); Serial.print(AcZ);
        //Serial.print(";"); Serial.print(Tmp/340.00+36.53);  //equation for temperature in degrees C from datasheet
        Serial.print(";"); Serial.print(Tmp);
        Serial.print(";"); Serial.print(GyX);
        Serial.print(":"); Serial.print(GyY);
        Serial.print(":"); Serial.print(GyZ);
        Serial.print("\r\n");
      

        /*
        File dataFile = SD.open(filename,FILE_WRITE);

        dataFile.print(millis());
        dataFile.print(""); dataFile.print(AcX);
        dataFile.print(" "); dataFile.print(AcY);
        dataFile.print(" "); dataFile.print(AcZ);
        dataFile.print(";"); dataFile.print(Tmp/340.00+36.53);  //equation for temperature in degrees C from datasheet
        dataFile.print(";"); dataFile.print(GyX);
        dataFile.print(" "); dataFile.print(GyY);
        dataFile.print(" "); dataFile.print(GyZ);
        dataFile.print("\n");


        dataFile.close();
        */
    }

    
/*

#include <SD.h>

// On the Ethernet Shield, CS is pin 4. Note that even if it's not
// used as the CS pin, the hardware CS pin (10 on most Arduino boards,
// 53 on the Mega) must be left as an output or the SD library
// functions will not work.
const int chipSelect = 10;

void setup()
{
 // Open serial communications and wait for port to open:
  Serial.begin(9600);
   while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }


  Serial.print("Initializing SD card...");
  // make sure that the default chip select pin is set to
  // output, even if you don't use it:
  pinMode(10, OUTPUT);
  
  // see if the card is present and can be initialized:
  if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    // don't do anything more:
    return;
  }
  Serial.println("card initialized.");
  char filename[16]; // make it long enough to hold your longest file name, plus a null terminator
  int n = 0;
  do
     {
        snprintf(filename, sizeof(filename), "data%03d.txt", n); // includes a three-digit sequence number in the file name
     }
  while(SD.exists(filename));
  //now filename[] contains the name of a file that doesn't exist

  }
void loop()
  {
  // make a string for assembling the data to log:
 char *filename;
 int sensorValue = analogRead(A0);
 float dataVoltage = sensorValue*(5.0/1023.0);
 File dataFile = SD.open(filename,FILE_WRITE);
 dataFile.println(dataVoltage);
 dataFile.close();
 Serial.println(dataVoltage);
 }

    */

