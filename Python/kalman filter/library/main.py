import numpy as np
import matplotlib.pyplot as plt
import transformations as tr
import yaml
import math
from esekf import *
import os


script_dir = os.path.dirname(__file__) #<-- absolute dir the script is in


def load_imu_parameters():
    f = open(script_dir+ '/data/params.yaml', 'r')
    yml = yaml.load(f.read())
    params = ImuParameters()
    params.frequency = yml['IMU.frequency']
    params.sigma_a_n = yml['IMU.acc_noise_sigma']  # m/sqrt(s^3)
    params.sigma_w_n = yml['IMU.gyro_noise_sigma']  # rad/sqrt(s)
    params.sigma_a_b = yml['IMU.acc_bias_sigma']     # m/sqrt(s^5)
    params.sigma_w_b = yml['IMU.gyro_bias_sigma']    # rad/sqrt(s^3)
    f.close()
    return params



def main():
    
    with open(script_dir+"/data/path.txt",'w') as outp:
        #outp.write("t;x;y;z;yaw;pitch;roll;vx;vy;vz\n")
        outp.write("yaw;pitch;roll\n")
        imu_data = np.loadtxt(script_dir+"/data/imu_noise.txt")


        imu_parameters = load_imu_parameters()

        init_nominal_state = np.zeros((19,))
        init_nominal_state[:10] = [0,0,0,1,0,0,0,0,0,0]              # init p, q, v  (speed 0, angle 0 position0)
        init_nominal_state[10:13] = 0                           # init ba
        init_nominal_state[13:16] = 0                           # init bg
        init_nominal_state[16:19] = np.array([0, 0, -9.8116])     # init g
        estimator = ESEKF(init_nominal_state, imu_parameters)

        
        

        
        for i in range(1, imu_data.shape[0]):
            timestamp = imu_data[i, 0]
            estimator.predict(imu_data[i, :])
            
            frame_pose = np.zeros(8,)
            frame_pose[0] = timestamp
            frame_pose[1:] = estimator.nominal_state[:7]

            
            frame_pose_euler = np.zeros(11,)
            frame_pose_euler[0] = timestamp
            frame_pose_euler[1:4] = estimator.nominal_state[0:3]
            frame_pose_euler[4:7] = tr.euler_from_quaternion(  estimator.nominal_state[3:7])
            frame_pose_euler[7:10] = estimator.nominal_state[7:10]
            
            #print(frame_pose_euler)
            
            outp.write("%f;%f;%f\n"%(frame_pose_euler[4],
                                frame_pose_euler[5],
                                frame_pose_euler[6]))
            #outp.write("%f;%f;%f;%f;%f;%f;%f;%f;%f;%f\n"%(frame_pose_euler[0],
            #                                     frame_pose_euler[1],
            #                                     frame_pose_euler[2],
            #                                     frame_pose_euler[3],
            #                                     frame_pose_euler[4],
            #                                     frame_pose_euler[5],
            #                                     frame_pose_euler[6],
            #                                     frame_pose_euler[7],
            #                                     frame_pose_euler[8],
            #                                     frame_pose_euler[9]))


if __name__ == '__main__':
    main()
