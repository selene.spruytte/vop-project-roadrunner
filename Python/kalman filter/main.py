import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import yaml




df = pd.read_table('nulpuntsmeting2019-02-25_03-42-21.txt', delimiter=";", header=0)
#df = pd.read_table('2019-02-25_03-11-33.txt', delimiter=";", header=0)

#____________calibration_______________

#ax             0.121568506568460
#ay            -0.000054966195790
#az           -10.933853359534035
#gx            -2.356726986203252
#gy             0.810309998351138
#gz            -0.311074784257679

#std:
#ax            0.063187709879635
#ay            0.007413919057394
#az            0.072669957426214
#gx            0.105667775979671
#gy            0.125925915462487
#gz            0.086400668088060





df['time'] = df['time'] - df['time'][0]  #set t to zero
df['deltatime'] =  df['time'] - df.shift(periods=1)['time'] #calculate time delta (needed for integration

df = df[ df['time']>=30] #account for impresions mpu in first 30 seconds
df = df[ df['time']<=40]


fs = 1./(df.mean()["deltatime"])
print("frequency is: %f"%fs)

print(df)
pd.set_option('precision', 15)
print(df.mean())
print(df.std())
pd.set_option('precision', 5)

#adjust for systematic bias (null point reference measurement
df['ax'] = df['ax']- 0.121568506568460
df['ay'] = df['ay']+ 0.000054966195790
df['az'] = df['az']+ 10.933853359534035 - 9.8116
df['gx'] = df['gx']+2.356726986203252
df['gy'] = df['gy']-0.810309998351138
df['gz'] = df['gz']+0.311074784257679



#_________chaining second program_______________

with open("library/data/params.yaml") as f:
     doc = yaml.load(f)

print(doc)

doc['IMU.frequency'] = "%f"%fs

with open("library/data/params.yaml", "w") as f:
    yaml.dump(doc, f)



os.system("python3 library/main.py")

df2 = pd.read_table('library/data/path.txt', delimiter=";", header=0)

df2['newind'] = df2.index + df.index[0]+1

df = df.join(df2.set_index('newind'))

print(df)



linearParam = 0.02

for i,row in df[1:].iterrows():
    
    row = df.loc[[i]]
    yaw = row['yaw'].values[0]
    roll = row['roll'].values[0]
    pitch = row['pitch'].values[0]
    
    c1 = np.cos(yaw)
    s1 = np.sin(yaw)
    c2 = np.cos(pitch)
    s2 = np.sin(pitch)
    c3 = np.cos(roll)
    s3 = np.sin(roll)
    
    ori = np.array( [[c1*c3-s1*c2*s3,   -c1*s3 -s1*c2*c3,    s1*s2],
                     [s1*c3 + c1*c2*s3, -s1*s3 + c1*c2*c3,  -c1*s2],
                     [s2*s3,            s2*c3,              c2]] )
    
    #print(ori)
    
    ax = row['ax'].values[0]
    ay = row['ay'].values[0]
    az = row['az'].values[0]
    
    ar = np.matmul( ori, np.array([[ax],[ay],[az]]))
    
    df.loc[[i],'arx'] =  ar[0,0]  #arealx
    df.loc[[i],'ary'] =  ar[1,0]
    df.loc[[i],'arz'] =  ar[2,0]
    
print(df)
#___________subtract gravity________________
    
df['arzg'] = df['arz'] + 9.8116 
#___________integrate height profile and distance travelled

df['vx'] = df['arx']*df['deltatime'].cumsum()
df['vy'] = df['ary']*df['deltatime'].cumsum()
df['vz'] = df['arzg']*df['deltatime'].cumsum()
df['px'] = df['vx']*df['deltatime'].cumsum()
df['py'] = df['vy']*df['deltatime'].cumsum()
df['pz'] = df['vz']*df['deltatime'].cumsum()

print(df)

#___________________plotting______________________




plt.figure(1)
plt.plot(df['time'],df['yaw']*180/np.pi,label='yaw')
plt.plot(df['time'],df['roll']*180/np.pi,label='pitch')
plt.plot(df['time'],df['pitch']*180/np.pi,label='roll')
plt.legend()
plt.show()
plt.figure(2)
plt.plot(df['time'],df['vx'],label='vx')
plt.plot(df['time'],df['vy'],label='vy')
plt.plot(df['time'],df['vz'],label='vz')
plt.legend()
plt.show()
plt.figure(3)
plt.plot(df['time'],df['ax'],label='ax')
plt.plot(df['time'],df['ay'],label='ay')
plt.plot(df['time'],df['az'],label='az')
plt.legend()
plt.show()
plt.figure(4)
plt.plot(df['time'],df['arx'],label='arx')
plt.plot(df['time'],df['ary'],label='ary')
plt.plot(df['time'],df['arz'],label='arz')
plt.legend()
plt.show()

plt.figure(5)
plt.plot(df['time'],df['pz'],label='pz')
plt.legend()
plt.show()


