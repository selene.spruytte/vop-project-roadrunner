#!/usr/bin/python
import smbus
import math
import time
from time import sleep
from signal import pause
 
# Register
power_mgmt_1 = 0x6b
power_mgmt_2 = 0x6c
 
def read_byte(reg):
    return bus.read_byte_data(address, reg)
 
def read_word(reg):
    h = bus.read_byte_data(address, reg)
    l = bus.read_byte_data(address, reg+1)
    value = (h << 8) + l
    return value
 
def read_word_2c(reg):
    val = read_word(reg)
    if (val >= 0x8000):
        return -((65535 - val) + 1)
    else:
        return val
 
def get_gyr_x():
     return float(read_word_2c(0x43)/131)
def get_gyr_y():
     return float(read_word_2c(0x45)/ 131)
def get_gyr_z():
     return float(read_word_2c(0x47)/ 131)
def get_acc_x():
    return float((read_word_2c(0x3b)/ 16384.0)*9.80665)
def get_acc_y():
    return float((read_word_2c(0x3d)/ 16384.0)*9.80665)
def get_acc_z():
    return float((read_word_2c(0x3f)/ 16384.0)*9.80665) 
     
bus = smbus.SMBus(1) # bus = smbus.SMBus(0) fuer Revision 1
address = 0x68       # via i2cdetect
bus.write_byte_data(address, power_mgmt_1, 0)
 
 #init
timestr = time.strftime("%Y-%m-%d_%H-%M-%S")

counter = 0;
t0 = time.time()



with open(timestr+".txt","a") as f:
    
    f.write("time;ax;ay;az;gx;gy;gz\n");
        
    #f.write("time;az;gx\n")
    
    while(True):
        
        string = "%.03f;%.5f;%d;%.5f;%.5f;%.5f;%.5f\n"%(time.time(),get_acc_x(),get_acc_y(),get_acc_z(),get_gyr_x(),get_gyr_y(),get_gyr_z());
        #time in seconds
        #t=time.time()
        #string = "%.03f;%.5f;%.5f\n"%(time.time(),get_acc_z(),get_gyr_x())
        f.write(string)
        #print(string);
        #counter +=1
        #print("%f"% ( counter/(t-t0)) )
        
        #print(string)
        #sleep(0.001)
    

 