import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

#df = pd.read_table('nulpuntsmeting2019-02-25_03-42-21.txt', delimiter=";", header=0)
df = pd.read_table('2019-02-25_03-11-33.txt', delimiter=";", header=0)

#____________calibration_______________

#ax             0.121568506568460
#ay            -0.000054966195790
#az           -10.933853359534035
#gx            -2.356726986203252
#gy             0.810309998351138
#gz            -0.311074784257679



df['time'] = df['time'] - df['time'][0]  #set t to zero
df['deltatime'] =  df['time'] - df.shift(periods=1)['time'] #calculate time delta (needed for integration

print(df)

#df = df[ df['time']>=30] #account for impresions mpu in first 30 seconds
#df = df[ df['time']<=35]
fs = 1./(df.mean()["deltatime"])

#print(df)
#pd.set_option('precision', 15)
#print(df.mean())
#pd.set_option('precision', 5)

#adjust for systematic bias (null point reference measurement
df['ax'] = df['ax']- 0.121568506568460
df['ay'] = df['ay']+ 0.000054966195790
df['az'] = df['az']+ 10.933853359534035 - 9.8116
df['wx'] = df['gx']+2.356726986203252
df['wy'] = df['gy']-0.810309998351138
df['wz'] = df['gz']+0.311074784257679
